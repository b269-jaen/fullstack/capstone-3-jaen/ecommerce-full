import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import {Container} from 'react-bootstrap';
import Products from './pages/Products'
import Register from './pages/Register'
import Login from './pages/Login'
import Logout from './pages/Logout'
import Error from './pages/Error'
import {useState, useEffect} from 'react'
import {UserProvider} from './UserContext'; 
import ProductView from './components/ProductView'
import AdminDashboard from './pages/AdminDashboard'
import Cart from './pages/Cart'
import Orders from './pages/Orders'
import AdminOrders from './pages/AdminOrders'
import './App.css';

import {BrowserRouter as Router, Route, Routes} from 'react-router-dom'

function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`,{
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {

      // User is logged in
      if(typeof data._id !== "undefined"){
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      }
      // User is logged out
      else{
        setUser({
          id: null,
          isAdmin: null
        })
      }
    })
  }, [])


  return (
    <>
      <UserProvider value={{user, setUser, unsetUser}}>
        <Router>
        <AppNavbar/>
        <Container>
          <Routes>
            < Route path="/" element={<Home/>} />
            < Route path="/register" element={<Register/>}/>
            < Route path="/products/:productId" element={<ProductView/>}/>
            < Route path="/cart" element={<Cart/>}/>
            < Route path="/orders" element={<Orders/>}/>
            < Route path="/AdminOrders" element={<AdminOrders/>}/>
            < Route path="/login" element={<Login/>}/>
            < Route path="/logout" element={<Logout/>}/>
            < Route path="/products" element={<Products/>}/>
            < Route path="/error" element={<Error/>}/>
            < Route path="/Dashboard" element={<AdminDashboard/>}/>
            < Route path="*" element={<Error />}/>
          </Routes>
        </Container>
        </Router>
      </UserProvider>
    </>
  );
}

export default App;
