import { useState, useEffect } from 'react';
import { Container, Card, Button, Row, Col, Form} from 'react-bootstrap';
import { useParams, useNavigate } from 'react-router-dom';
import { useContext } from 'react';
import UserContext from '../UserContext';
import { Link } from 'react-router-dom';
import Swal from 'sweetalert2';


export default function ProductView() {

  const { productId } = useParams();
  const navigate = useNavigate();
  const { user } = useContext(UserContext);
  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState(0);
  const [quantity, setQuantity] = useState(1);
  const [img, setImg] = useState("");


  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
      .then(res => res.json())
      .then(data => {
        console.log(data);

        setName(data.name);
        setDescription(data.description);
        setPrice(data.price);
        setImg(data.img)
      })
  }, [productId])

  const addToCart = (productId, quantity) => {
    fetch(`${process.env.REACT_APP_API_URL}/cart/add`,{
      method: 'POST',
      headers: {'Content-Type': 'application/json', Authorization: `Bearer ${localStorage.getItem('token')}`},
      body: JSON.stringify({
        productId: productId,
        quantity: quantity
      })
    })
    .then(res => res.json())
    .then(data => {
      if(data !== null){
        Swal.fire("Added to cart")
        navigate('/products')
      }else{
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Something went wrong!, try again',
        })
      }
    })
  };


  const checkOut = (productId, quantity) => {

    Swal.fire({
      title: 'Checkout?',
      text: "You can cancel your order in the order history page!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, checkout!'
    }).then((result) => {
      if (result.isConfirmed) {
        fetch(`${process.env.REACT_APP_API_URL}/orders/create`, {
          method: 'POST',
          headers: {'Content-Type': 'application/json', Authorization: `Bearer ${localStorage.getItem('token')}`},
          body: JSON.stringify({
            productId: productId,
            quantity: quantity
          })
        })
        .then(res => res.json())
        .then(data => {
          console.log(data);
          Swal.fire({
            icon: 'success',
            title: 'Product has been checked out',
            text: 'Proceed to My Orders to check order history',
          })
          navigate('/orders')
        })
      }
    })

    
  }

  return (
    <Container>
      <Row>
        <Col lg={{ span: 6, offset: 3 }} className="mt-5">
          <Card className="productViewCard">
            <Card.Body className="text-center">
              <img src={img} alt="Product 1" className="viewproduct-image" />
              <Card.Title>{name}</Card.Title>
              <Card.Subtitle>Description:</Card.Subtitle>
              <Card.Text>{description}</Card.Text>
              <Card.Subtitle>Price:</Card.Subtitle>
              <Card.Text>PhP {price}</Card.Text>

              <Form.Group controlId="quantity">
                <Form.Label>Quantity:</Form.Label>
                <Form.Control
                  type="number"
                  min={1}
                  value={quantity}
                  onChange={(e) => setQuantity(parseInt(e.target.value))}
                />
              </Form.Group>

              {
                (user.id !== null) ?
                  <>
                  <Button className="mt-3 mx-2" variant="primary" onClick={() => addToCart(productId, quantity)} >Add to Cart</Button>
                  <Button className="mt-3 mx-2" variant="primary" onClick={() => checkOut(productId, quantity)} >Check Out</Button>
                  </>
                  :
                  <Button className="btn btn-danger mt-3" as={Link} to="/login">Log in to buy</Button>
              }
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  )
}
