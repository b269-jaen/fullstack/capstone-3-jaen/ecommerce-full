import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import {Link, NavLink} from 'react-router-dom';
import {useContext} from 'react';
import UserContext from '../UserContext';
import {Container} from 'react-bootstrap';

export default function AppNavbar() {
  // to store user information stored in the login page
  // const [user, setUser] = useState(localStorage.getItem('email'));

  const {user} = useContext(UserContext);


  return (
    <Navbar expand="lg" className="mb-5 navbar">
        <Container>
          <Navbar.Brand as={Link} to="/" style={{ color: 'white' }}>
            S-L
          </Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="me-auto">
              <Nav.Link as={NavLink} to="/" style={{ color: 'white' }}>
                Home
              </Nav.Link>
              <Nav.Link as={NavLink} to="/products" style={{ color: 'white' }}>
                Products
              </Nav.Link>
              {user.id !== null ? (
                user.isAdmin === true ? (
                  <>
                    <Nav.Link as={NavLink} to="/Dashboard" style={{ color: 'white' }}>
                      Admin Dashboard
                    </Nav.Link>
                  </>
                ) : (
                  <>
                    <Nav.Link as={NavLink} to="/cart" style={{ color: 'white' }}>
                      My Cart
                    </Nav.Link>
                    <Nav.Link as={NavLink} to="/orders" style={{ color: 'white' }}>
                      My Orders
                    </Nav.Link>
                  </>
                )
              ) : null}
            </Nav>
            <Nav className="ml-auto">
              {user.id !== null ? (
                <Nav.Link as={NavLink} to="/logout" style={{ color: 'white' }}>
                  Logout
                </Nav.Link>
              ) : (
                <>
                  <Nav.Link as={NavLink} to="/register" style={{ color: 'white' }}>
                    Register
                  </Nav.Link>
                  <Nav.Link as={NavLink} to="/login" style={{ color: 'white' }}>
                    Login
                  </Nav.Link>
                </>
              )}
            </Nav>
          </Navbar.Collapse>
          </Container>
        </Navbar>
      );
}
