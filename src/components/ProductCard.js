import { Button, Col, Card } from 'react-bootstrap';
// import {useState} from 'react';
import {useContext} from 'react';
import {Link} from 'react-router-dom';
import UserContext from '../UserContext';


export default function ProductCard({product}) {

    const {user} = useContext(UserContext);
	
	const {name, description, price, _id, note, img} = product;



return (
        <Col xs={12} sm={6} md={4} className=" mt-3 mb-3">
            <Card className="cardHighlight p-0">
            	<Card.Header className="text-center">{note}</Card.Header>
                <Card.Body>
                    <img src={img} alt="Product" className="showproduct-image" />
                    <Card.Title><h4>{name}</h4></Card.Title>
                    <Card.Subtitle>Description</Card.Subtitle>
                    <Card.Text>{description}</Card.Text>
                    <Card.Subtitle>Price</Card.Subtitle>
                    <Card.Text>PhP {price}</Card.Text>

                    {user.isAdmin === true ? null :<Button className="cardButton" as={Link} to={`/products/${_id}`}>Details</Button>}
                </Card.Body>
            </Card>
        </Col>
    )
}
