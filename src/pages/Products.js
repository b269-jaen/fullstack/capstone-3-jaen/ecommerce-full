import React, {useState, useEffect} from 'react';
import ProductCard from '../components/ProductCard';
import {Container, Row} from 'react-bootstrap';

export default function Products(){

	const [products, setProducts] = useState([]);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/all`)
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setProducts(data.map(product => {
				return(
					<ProductCard key={product._id} product={product} />
				)
			}))
		})
	}, []);

	return(
		<>
			<Container>
				<Row className="mt-3 mb-3">
					{products}
				</Row>
			</Container>
		</>
	)
}
