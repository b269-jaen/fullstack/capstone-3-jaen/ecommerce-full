import React, { useEffect, useState } from "react";
import {useContext} from 'react';
import UserContext from '../UserContext';
import {Navigate} from 'react-router-dom';
import { Button} from 'react-bootstrap';
import Swal from 'sweetalert2';

const Orders = () => {
  const [orders, setOrders] = useState([]);
  const {user} = useContext(UserContext);
  const [completedOrders, setCompletedOrders] = useState([]);


  const fetchPending = () => {
      fetch(`${process.env.REACT_APP_API_URL}/orders/admin/pending`, {
      headers: {'Content-Type': 'application/json', Authorization: `Bearer ${localStorage.getItem('token')}`}
    })
    .then(res => res.json())
    .then(data => setOrders(data))
    .catch(error => {
        console.error("Error fetching orders data:", error);
      });
    }


    const fetchApproved = () => {
      fetch(`${process.env.REACT_APP_API_URL}/orders/admin/approved`, {
      headers: {'Content-Type': 'application/json', Authorization: `Bearer ${localStorage.getItem('token')}`}
    })
    .then(res => res.json())
    .then(data => setCompletedOrders(data))
    .catch(error => {
        console.error("Error fetching orders data:", error);
      });
    }


  useEffect(() => {

    fetchPending();
    fetchApproved();

    const interval1 = setInterval(fetchPending, 2000);
    const interval2 = setInterval(fetchApproved, 2000);

    return () => {
      clearInterval(interval1);
      clearInterval(interval2);
    };

  }, []);


  const handleCancelOrder = (orderId) => {
    fetch(`${process.env.REACT_APP_API_URL}/orders/admin/cancel`, {
      method: 'DELETE',
      headers: {'Content-Type': 'application/json', Authorization: `Bearer ${localStorage.getItem('token')}`},
      body: JSON.stringify({
        orderId: orderId
      })
    })
    .then(res => res.json())
    .then(data => {
      if(data){
        Swal.fire({
          position: 'center',
          icon: 'success',
          title: 'Order Canceled',
          showConfirmButton: false,
          timer: 1500
        })
        fetchPending();
      }

    })
  }

  const acceptOrder = (orderId) => {
    fetch(`${process.env.REACT_APP_API_URL}/orders/admin/approve`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify({
        orderId: orderId
      })
    })
    .then((res) => res.json())
    .then((data) => {
      if(data){
        Swal.fire({
          position: 'center',
          icon: 'success',
          title: 'Order Accepted',
          showConfirmButton: false,
          timer: 1500
        })

        fetchPending();
        fetchApproved();

      }else{
        console.log(data)
      }
    })
  }



  return (

    (user.isAdmin !== true) ? ( <Navigate to="/error"/>)
    :
      <div>
          <div className="order-page-container">
            <h1 className="order-page-title">Pending Orders</h1>
            {orders.length > 0 ? (
              <ul className="order-list">
                {orders.map(order => (
                  <li key={order._id} className="order-item">
                    <div className="order-info">
                      <h2 className="order-id">Order ID: {order._id}</h2>
                      <h3 className="order-id">Status: {order.status}</h3>
                      <p className="user-id">User ID: {order.userId}</p>
                      <p className="total-amount">Total Amount: ${order.totalAmount}</p>
                      <p className="purchased-on">Purchased On: {new Date(order.purchasedOn).toLocaleString()}</p>
                    </div>
                    <div className="products-list">
                      <h3 className="products-title">Products:</h3>
                      <ul className="products-ul">
                        {order.products.map(product => (
                          <li key={product.productId} className="product-item">
                            <h4 className="product-name">Product Name: {product.productName}</h4>
                            <p className="quantity">Quantity: {product.quantity}</p>
                            <p className="subtotal">Subtotal: ${product.subTotal}</p>
                            <img src={product.img} alt={product.productName} className="product-image" />
                          </li>
                        ))}
                      </ul>
                    </div>
                    <Button onClick={() => handleCancelOrder(order._id)}
                      className="cancel-order-btn mx-2 my-2" variant="danger">
                      Decline Order
                    </Button>
                    <Button onClick={() => acceptOrder(order._id)} variant="success">
                      Accept Order
                    </Button>
                  </li>
                ))}
              </ul>
            ) : (
              <p className="no-orders-msg">No orders found.</p>
            )}
          </div>

          <div className="completed-order-page-container">
            <h1 className="order-page-title">Approved Orders</h1>
            {completedOrders.length > 0 ? (
              <ul className="order-list">
                {completedOrders.map(order => (
                  <li key={order._id} className="order-item">
                    <div className="order-info">
                      <h2 className="order-id">Order ID: {order._id}</h2>
                      <h3 className="order-id">Status: {order.status}</h3>
                      <p className="user-id">User ID: {order.userId}</p>
                      <p className="total-amount">Total Amount: ${order.totalAmount}</p>
                      <p className="purchased-on">Purchased On: {new Date(order.purchasedOn).toLocaleString()}</p>
                    </div>
                    <div className="products-list">
                      <h3 className="products-title">Products:</h3>
                      <ul className="products-ul">
                      {order.products.map(product => (
                        <li key={product.productId} className="product-item">
                          <h4 className="product-name">Product Name: {product.productName}</h4>
                          <p className="quantity">Quantity: {product.quantity}</p>
                          <p className="subtotal">Subtotal: ${product.subTotal}</p>
                          <img src={product.img} alt={product.productName} className="product-image" />
                        </li>
                      ))}
                      </ul>
                    </div>
                  </li>
                ))}
              </ul>
            ) : ( <p className="no-orders-msg">No completed orders found.</p> 
            )}
          </div>
      </div>

  );                  


};

export default Orders;
