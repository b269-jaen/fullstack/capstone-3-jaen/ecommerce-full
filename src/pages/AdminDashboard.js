import React, { useState, useEffect} from "react";
import {Navigate, useNavigate } from 'react-router-dom';
import { Button, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper, TextField, Dialog, DialogTitle, DialogContent, DialogActions } from "@material-ui/core";
import Swal from 'sweetalert2';
import {useContext} from 'react';
import UserContext from '../UserContext';

const AdminDashboard = () => {

  const {user} = useContext(UserContext);
  const navigate = useNavigate();
  const [products, setProducts] = useState([]);
  const [editProductId, setEditProductId] = useState("");
  const [editProductName, setEditProductName] = useState("");
  const [editProductDescription, setEditProductDescription] = useState("");
  const [editProductPrice, setEditProductPrice] = useState("");
  const [editProductNote, setEditProductNote] = useState("");
  const [editProductImg, setEditProductImg] = useState("");
  const [editProductAvailability, setEditProductAvailability] = useState("");
  const [openEditDialog, setOpenEditDialog] = useState(false);
  const [openAddDialog, setOpenAddDialog] = useState(false);
  const [newProductName, setNewProductName] = useState(""); 
  const [newProductDescription, setNewProductDescription] = useState(""); 
  const [newProductPrice, setNewProductPrice] = useState(""); 
  const [newProductNote, setNewProductNote] = useState(""); 
  const [newProductImg, setNewProductImg] = useState(""); 

  // Fetch products from backend server
  useEffect(() => {
    const fetchProducts = () => {
      fetch(`${process.env.REACT_APP_API_URL}/products/allAdmin`, {
        headers: {
          'Content-Type': 'application/json'
        }
      })
        .then((response) => response.json())
        .then(data => {
          setProducts(data)
        })
        .catch(error => {
          console.error("Error fetching products:", error);
        });
    }

    fetchProducts(); // Fetch products initially

    // Set up an interval to fetch products every 2 seconds
    const intervalId = setInterval(fetchProducts, 2000);

    // Clean up the interval on unmount
    return () => {
      clearInterval(intervalId);
    };
  }, []);

  // Function to handle adding new product
  const handleAddProduct = () => {
    setOpenAddDialog(true);

  };

  // Function when add button is pressed
  const createProduct = () => {
    fetch(`${process.env.REACT_APP_API_URL}/products/admin/create`,{
          method: 'POST',
          headers: {'Content-Type': 'application/json', Authorization: `Bearer ${localStorage.getItem('token')}`},
          body: JSON.stringify({
            name: newProductName,
            description: newProductDescription,
            price: newProductPrice,
            note: newProductNote,
            img: newProductImg
          })
        })
        .then(res => res.json())
        .then(data => {

          console.log(data);

          if(data){
            Swal.fire("Succesfully added new product")
            setOpenAddDialog(false);
            setNewProductName("")
            setNewProductDescription("")
            setNewProductPrice("")
            setNewProductNote("")
            setNewProductImg("")
          }else{
            Swal.fire({
              icon: 'error',
              title: 'Oops...',
              text: 'Product creation failed, try again',
              footer: '<a href="">Why do I have this issue?</a>'
            })
          }

        })
  }


  // Function when close button is pressed
  const handleCloseAddDialog = () => {
    setOpenAddDialog(false);
  }

  // Function to handle showing user orders
  const handleShowUserOrders = () => {
    navigate('/AdminOrders');
  };

  // Function to handle editing a product
  const handleEditProduct = (product) => {
    setEditProductId(product._id);
    setEditProductName(product.name);
    setEditProductDescription(product.description);
    setEditProductPrice(product.price);
    setEditProductNote(product.note);
    setEditProductImg(product.img);
    setEditProductAvailability(product.isActive);
    setOpenEditDialog(true);
  };

  // Function to handle updating a product
  const handleUpdateProduct = () => {
    const updatedProduct = {
      productId: editProductId,
      name: editProductName,
      description: editProductDescription,
      price: editProductPrice,
      isActive: editProductAvailability,
      note: editProductNote,
      img: editProductImg
    };

    fetch(`${process.env.REACT_APP_API_URL}/products/update/${editProductId}`, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify(updatedProduct)
    })
      .then(res => res.json())
      .then((data) => {
        Swal.fire("Product Update Succesful");
        setOpenEditDialog(false);

      })
      .catch(error => {
        console.error("Error updating product:", error);
      });
  };

  // Function to handle canceling the edit dialog
  const handleCancelEditDialog = () => {
    setOpenEditDialog(false);
  };

  const disable = (productId) => {
    fetch(`${process.env.REACT_APP_API_URL}/products/admin/archive`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify({
        id: productId
      })
      })
      .then(res => res.json())
      .then((data) => {
        Swal.fire("Product Disabled");
        
        })
        .catch(error => {
        console.error("Error disabling product:", error);
        });
        };
   
  return (

    (user.isAdmin !== true)?
    <Navigate to="/error"/>
    :
    <div>
      <h1>Admin Dashboard</h1>
      <Button variant="contained" color="primary" className="mx-2 my-2" onClick={handleAddProduct}>
      Add Product
      </Button>
      <Button variant="contained" color="secondary" className="mx-2 my-2" onClick={handleShowUserOrders}>
      Show User Orders
      </Button>
      <TableContainer component={Paper}>
        <Table>
        <TableHead>
          <TableRow>
            <TableCell>Name</TableCell>
            <TableCell>Description</TableCell>
            <TableCell>Price</TableCell>
            <TableCell>Note</TableCell>
            <TableCell>Image</TableCell>
            <TableCell>Availability</TableCell>
            <TableCell>Action</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {products.map((product) => (
          <TableRow key={product._id}>
            <TableCell>{product.name}</TableCell>
            <TableCell>{product.description}</TableCell>
            <TableCell>{product.price}</TableCell>
            <TableCell>{product.note}</TableCell>
            <TableCell><img src={product.img} className="product-image"/></TableCell>
            <TableCell>{product.isActive ? "Available" : "Not Available"}</TableCell>
            <TableCell>
            <Button variant="contained" color="primary" className="mx-2" onClick={() => handleEditProduct(product)}>
            Edit
            </Button>
            <Button variant="contained" color="secondary" className="mx-2" onClick={() => disable(product._id)}>
            Disable
            </Button>
            </TableCell>
          </TableRow>
          ))}
        </TableBody>
        </Table>
      </TableContainer>
      {/* Add Product Dialog */}
        <Dialog open={openAddDialog} onClose={handleCloseAddDialog}>
          <DialogTitle>Add Product</DialogTitle>
          <DialogContent>
            <TextField
              label="Name"
              value={newProductName}
              onChange={(e) => setNewProductName(e.target.value)}
              fullWidth
              margin="dense"
            />
            <TextField
              label="Description"
              value={newProductDescription}
              onChange={(e) => setNewProductDescription(e.target.value)}
              fullWidth
              margin="dense"
            />
            <TextField
              label="Price"
              value={newProductPrice}
              onChange={(e) => setNewProductPrice(e.target.value)}
              fullWidth
              margin="dense"
            />
            <TextField
              label="Note"
              value={newProductNote}
              onChange={(e) => setNewProductNote(e.target.value)}
              fullWidth
              margin="dense"
            />
            <TextField
              label="Image"
              value={newProductImg}
              onChange={(e) => setNewProductImg(e.target.value)}
              fullWidth
              margin="dense"
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={handleCloseAddDialog} color="primary">
              Cancel
            </Button>
            <Button onClick={createProduct} color="primary">
              Add
            </Button>
          </DialogActions>
        </Dialog>
      {/*Dialog box for editing product*/}
      <Dialog open={openEditDialog} onClose={handleCancelEditDialog}>
        <DialogTitle>Edit Product</DialogTitle>
        <DialogContent>
          <TextField
          label="Name"
          value={editProductName}
          onChange={(e) => setEditProductName(e.target.value)}
          />
          <TextField
          label="Description"
          value={editProductDescription}
          onChange={(e) => setEditProductDescription(e.target.value)}
          />
          <TextField
          label="Price"
          value={editProductPrice}
          onChange={(e) => setEditProductPrice(e.target.value)}
          />
          <TextField
          label="Note"
          value={editProductNote}
          onChange={(e) => setEditProductNote(e.target.value)}
          />
          <TextField
          label="Image"
          value={editProductImg}
          onChange={(e) => setEditProductImg(e.target.value)}
          />
          <TextField
          label="Availability"
          value={editProductAvailability}
          onChange={(e) => setEditProductAvailability(e.target.value)}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={handleUpdateProduct} color="primary">
          Update
          </Button>
          <Button onClick={handleCancelEditDialog} color="secondary">
          Cancel
          </Button>
        </DialogActions>
      </Dialog>
    </div>
    );
    };

  export default AdminDashboard;