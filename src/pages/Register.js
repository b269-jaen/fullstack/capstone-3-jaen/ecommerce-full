import { Form, Button } from 'react-bootstrap';
import {useState, useEffect, useContext} from 'react';
import {Navigate} from 'react-router-dom';
import UserContext from '../UserContext';
import {useNavigate} from 'react-router-dom';
import Swal from 'sweetalert2';


export default function Register() {

    // to store and manage value of the input fields
    const [firstName, setFirstName] = useState("")
    const [lastName, setLastName] = useState("")
    const [mobileNo, setMobileNo] = useState("")

    const [email, setEmail] = useState("");
    const [password1, setPassword1] = useState("");
    const [password2, setPassword2] = useState("");

    const navigate = useNavigate();

    // to determine whether the submit button is enabled or not
    const [isActive, setIsActive] = useState(false);

    const {user, setUser} = useContext(UserContext);

    useEffect(() => {

        if(email !== "" && password1 !== "" && password2 !==""  && password1 === password2 && mobileNo.length >= 11 && firstName !== "" && lastName !== ""){
            setIsActive(true);
        }else{
            setIsActive(false);

        }

    }, [email, password1, password2, firstName, lastName, mobileNo])

    // function to simulate user registration
    function registerUser(e) {
        e.preventDefault();

      
        
        fetch(`${process.env.REACT_APP_API_URL}/users/register`,{
            method: "POST",
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify({
                firstName: firstName,
                lastName: lastName,
                mobileNo: mobileNo,
                email: email,
                password: password1
                })
            }) 
            .then(res => res.json())
            .then(data => {
                if(data === false){
                    Swal.fire({
                      icon: 'error',
                      title: 'User email already exist',
                      text: "Email is already existing, please register a new one or log in instead",
                      showCancelButton: true,
                      confirmButtonText: 'Go to Login'
                    }).then((result) => {
                        if(result.isConfirmed){
                            navigate('/login');
                        }
                    }) 
                }else{
                    Swal.fire({
                    title: "Registration successful",
                    icon: "success",
                    text: `Welcome to Shop-Lift ${firstName}!`
                    })
                    navigate('/login');

                    // Clear input fields
                    setEmail("");
                    setPassword1("");
                    setPassword2("");
                }
            })        
    }



    return (
        // s54 activity
        (user.id !== null)?
        <Navigate to="/products"/>
        :
        <Form onSubmit= {(e) => registerUser(e)}>
            <Form.Group controlId="userFirstName">
                <Form.Label>First Name</Form.Label>
                <Form.Control 
                    type="text" 
                    placeholder="Enter First Name" 
                    value={firstName}
                    onChange={e => setFirstName(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="userLastName">
                <Form.Label>Last Name</Form.Label>
                <Form.Control 
                    type="text" 
                    placeholder="Enter Last Name" 
                    value={lastName}
                    onChange={e => setLastName(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="userPhone">
                <Form.Label>Mobile Number</Form.Label>
                <Form.Control 
                    type="text" 
                    placeholder="Enter Mobile Number" 
                    value={mobileNo}
                    onChange={e => setMobileNo(e.target.value)}
                    required
                />
            </Form.Group>

            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
	                type="email" 
	                placeholder="Enter email" 
                    value={email}
                    onChange={e => setEmail(e.target.value)}
	                required
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group controlId="password1">
                <Form.Label>Password</Form.Label>
                <Form.Control 
	                type="password" 
	                placeholder="Password"
                    value={password1}
                    onChange={e => setPassword1(e.target.value)}
	                required
                />
            </Form.Group>

            <Form.Group controlId="password2">
                <Form.Label>Verify Password</Form.Label>
                <Form.Control 
	                type="password" 
	                placeholder="Verify Password"
                    value={password2} 
                    onChange={e => setPassword2(e.target.value)}
	                required
                />
            </Form.Group>
            {isActive ? 

            <Button className="mt-3" variant="primary" type="submit" id="submitBtn">
                Submit
            </Button>
            :
            <Button className="mt-3" variant="danger" type="submit" id="submitBtn" disabled>
                Submit
            </Button>
            
            }
            
        </Form>
    )

}
