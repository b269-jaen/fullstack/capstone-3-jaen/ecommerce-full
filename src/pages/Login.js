import {useState, useEffect, useContext} from 'react';
import {Form, Button} from 'react-bootstrap';
import Swal from 'sweetalert2';
import {useNavigate} from 'react-router-dom';
import {Navigate} from 'react-router-dom';
import UserContext from '../UserContext';

export default function Login(){

    
    const {user, setUser} = useContext(UserContext);
    const navigate = useNavigate();
  
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    // to determine whether submit button is enabled or not
    const [isActive, setIsActive] = useState(false);


    function authenticate(e){
        e.preventDefault()

        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            // We will receive either a token or an error response.
            console.log(data);

            
            if(typeof data.access !== "undefined") {
                // The JWT will be used to retrieve user information across the the whole frontend application and storing it in the localStorage will allow ease of access to the user's information
                localStorage.setItem('token', data.access);
                retrieveUserDetails(data.access);

            }else if (data === false){
                Swal.fire({
                    title: "Authentication Failed!",
                    icon: "error",
                    text: "User is not yet registered, please register first."
                })

                navigate('/register');
            }else if(data === true){
                Swal.fire({
                    title: "Incorrect Password",
                    icon: "error",
                    text: "The password you entered is incorrect. Please try again"
                })
            }
    });
        setEmail('')
        setPassword('')
        
};
    const retrieveUserDetails = (token) => {
            // The token will be sent as part of the request's header information
            // We put "Bearer" in front of the token to follow implementation standards for JWTs
            fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            })
            .then(res => res.json())
            .then(data => {
                console.log(data);
                // Global user state for validation accross the whole app
                // Changes the global "user" state to store the "id" and the "isAdmin" property of the user which will be used for validation across the whole application
                setUser({
                    id: data._id,
                    isAdmin: data.isAdmin,
                    firstName: data.firstName
                })
                localStorage.setItem('isAdmin', data.isAdmin);
                localStorage.setItem('firstName', data.firstName);
                Swal.fire({
                    title: "Login Successful!",
                    icon: "success",
                    text: `Welcome to Shop-Lift ${data.firstName}`
                })

            })
        };

    useEffect(() => {
        if((email !== '' && password !== '')){
            setIsActive(true)
        } else {
            setIsActive(false)
        }
    }, [email, password])

    useEffect(() => {
        console.log(user); // Verify if user state is being updated correctly
    }, [user]); // Add user as a dependency for useEffect




    return(
        (user.id !== null) ?
        <Navigate to="/products" />
        :
        <Form onSubmit={e => authenticate(e)}>
            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
                    type="email" 
                    placeholder="Enter email"
                    value={email}
                    onChange={e => setEmail(e.target.value)}
                    required
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group controlId="password">
                <Form.Label>Password</Form.Label>
                <Form.Control 
                    type="password" 
                    placeholder="Password"
                    value={password}
                    onChange={e => setPassword(e.target.value)}
                    required
                />
            </Form.Group>

            {   isActive ?
                <Button variant="primary" type="submit" id="submitBtn" className="mt-3">
                    Submit
                </Button>
                :
                <Button variant="danger" type="submit" id="submitBtn" className="mt-3" disabled>
                    Submit
                </Button>
            }
        </Form>
    )
};