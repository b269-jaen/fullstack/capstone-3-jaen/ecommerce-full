
import { Button, Row, Col} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function Error(){

	const data = {
		title: "Error 404 - Page not found.",
		content: "The page you are looking for cannot be found.",
		destination: "/",
		label: "Back to Home"
	}

	return (
			<Row>
	        	<Col className="p-5">
	                <h1>{data.title}</h1>
	                <p>{data.content}</p>
	                <Button variant="primary" as={Link} to={data.destination} >{data.label}</Button>
	            </Col>
        	</Row>
		)
}