import React, { useState, useEffect } from 'react';
import { Button} from 'react-bootstrap';
import Swal from 'sweetalert2';
import {useContext} from 'react';
import UserContext from '../UserContext';
import {Navigate} from 'react-router-dom';

const OrderPage = () => {

  const [orders, setOrders] = useState([]);
  const {user} = useContext(UserContext);

  useEffect(() => {
    // Fetch orders from backend
    fetch(`${process.env.REACT_APP_API_URL}/orders/user`, {
      headers: {'Content-Type': 'application/json', Authorization: `Bearer ${localStorage.getItem('token')}`}     
    })
    .then(res => res.json())
    .then(data => {
      setOrders(data);
    })
    .catch(error => {
        console.error('Failed to fetch orders:', error);
      });
  }, []);

  const fetchOrderData = () => {
    fetch(`${process.env.REACT_APP_API_URL}/orders/user`, {
          headers: {'Content-Type': 'application/json', Authorization: `Bearer ${localStorage.getItem('token')}`}     
        })
        .then(res => res.json())
        .then(data => {
          setOrders(data);
        })
        .catch(error => {
            console.error('Failed to fetch orders:', error);
          });
  }


  const handleCancelOrder = (orderId) => {
    console.log(orderId);
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, cancel it!'
    }).then((result) => {
      if (result.isConfirmed) {

        fetch(`${process.env.REACT_APP_API_URL}/orders/cancel`, {
          method: 'DELETE',
          headers: {'Content-Type': 'application/json', Authorization: `Bearer ${localStorage.getItem('token')}`},
          body: JSON.stringify({
           orderId: orderId
          })
        })
        .then(res => res.json())
        .then(data => {
          console.log(data)
        })

        Swal.fire(
          'Deleted!',
          'Your order has been canceled.',
          'success'
        )

        fetchOrderData();
      }
      })
    }

  return (
    (user.isAdmin === true) ?
    <Navigate to='/error'/>
    :
    <div className="order-page-container">
      <h1 className="order-page-title">Order History</h1>
      {orders.length > 0 ? (
        <ul className="order-list">
          {orders.map(order => (
            <li key={order._id} className="order-item">
              <div className="order-info">
                <h2 className="order-id">Order ID: {order._id}</h2>
                <h3 className="order-id">Status: {order.status} </h3>
                <p className="user-id">User ID: {order.userId}</p>
                <p className="total-amount">Total Amount: ${order.totalAmount}</p>
                <p className="purchased-on">Purchased On: {new Date(order.purchasedOn).toLocaleString()}</p>
              </div>
              <div className="products-list">
                <h3 className="products-title">Products:</h3>
                <ul className="products-ul">
                  {order.products.map(product => (
                    <li key={product.productId} className="product-item">
                      <h4 className="product-name">Product Name: {product.productName}</h4>
                      <p className="quantity">Quantity: {product.quantity}</p>
                      <p className="subtotal">Subtotal: ${product.subTotal}</p>
                      <img src={product.img} alt={product.productName} className="product-image" />
                    </li>
                  ))}
                </ul>
              </div>

              {order.status !== 'APPROVED' ? 
              <Button onClick={() => handleCancelOrder(order._id)}
                className="cancel-order-btn my-3" variant="danger">
                Cancel Order
              </Button>
              :
              null
            }
        
            </li>
          ))}
        </ul>
      ) : (
        <p className="no-orders-msg">No orders found.</p>
      )}
    </div>
  );
};

export default OrderPage;
