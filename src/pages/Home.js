import {Container, Row, Col} from 'react-bootstrap';



export default function Home() {


	return(
		<>
		<Container className="justify-content-center">
			<Row>
				<Col xs={12}>
					<header className="header">
				        <div className="hero">
				          <h1 className="hero-title">Shop-Lift</h1>
				          <p className="hero-subtitle">
				            Find the Best Deals on Computer Peripherals
				          </p>
				          <a href="/products" className="hero-button">
				            Shop Now
				          </a>
				        </div>
					</header>
				</Col>
				<Col xs={12}>
			      <main className="main-content">
				        <section className="featured-products">
				          <h2 className="section-title">Featured Products</h2>
				          <div className="product-list justify-content-center">
				          <Row>
				          	<Col xs={12} md={4} lg={4}>
					            <div className="product-card">
					              <img src="https://cdn.shopify.com/s/files/1/0046/4109/5726/products/3408-c_600x.jpg?v=1623846847" alt="Product 1" className="product-image" />
					              <h3 className="product-title">Gaming Mouse</h3>
					              <p className="product-price">P 3,850</p>
					              <a href="/products" className="product-button">
					                Add to Cart
					              </a>
					            </div>
					        </Col>
					        <Col xs={12} md={4} lg={4}>
					            <div className="product-card">
					              <img src="https://m.media-amazon.com/images/I/61SjS2rzDzL.jpg" alt="Product 2" className="product-image" />
					              <h3 className="product-title">Mechanical Keyboard</h3>
					              <p className="product-price">P 3,200</p>
					              <a href="/products" className="product-button">
					                Add to Cart
					              </a>
					            </div>
					        </Col>
					        <Col xs={12} md={4} lg={4}>
					            <div className="product-card">
					              <img src="https://shopee.ph/blog/wp-content/uploads/2022/11/IMG_3183.jpg" alt="Product 3" className="product-image" />
					              <h3 className="product-title">Wireless Headset</h3>
					              <p className="product-price">P 3,400</p>
					              <a href="/products" className="product-button">
					                Add to Cart
					              </a>
					            </div>
					        </Col>
					      </Row>
				          </div>
				        </section>
				        <section className="newsletter">
				          <h2 className="section-title">Subscribe to Our Newsletter</h2>
				          <p className="newsletter-text">
				            Sign up to receive updates on new products, promotions, and more.
				          </p>
				          {/*<Row>*/}
				          <form action="/register" className="newsletter-form">
				          <Row>
				          	<Col xs={12} md={8}>
				            <input
				              type="email"
				              placeholder="Enter your email address"
				              className="newsletter-input justify-content-center"
				            />
				            </Col>
				            <Col xs={12} md={4}>
				            <button type="submit" className="newsletter-button justify-content-end">
				              Subscribe
				            </button>
				           	</Col>
				           </Row>				          				          
				          </form>
				         {/*</Row>*/}				          
				        </section>
				    </main>
			   
			      <footer className="footer">
			        <p className="footer-text">
			          © 2023 S-L - Shop-Lift. All rights reserved.
			        </p>
			      </footer>
			    </Col>	  
			</Row>    		
		 </Container>
		</>
	)
}