import React, { useEffect, useState } from 'react';
import {Button} from 'react-bootstrap';
import Swal from 'sweetalert2';
import {useNavigate } from 'react-router-dom';
import {Container, Row, Col} from 'react-bootstrap';



export default function MyCartPage () {
  const [cart, setCart] = useState(null);
  const navigate = useNavigate();


  useEffect(() => {
    // Fetch cart data from backend
    fetch(`${process.env.REACT_APP_API_URL}/cart/show`, {
      headers: { 'Content-Type': 'application/json', Authorization: `Bearer ${localStorage.getItem('token')}` }
    })
      .then(res => res.json())
      .then(data => {
        setCart(data)
        console.log(data)


        if(data === false){
          navigate('/error');
        }
      })
  }, [navigate]);


  const fetchCartData = () => {
      // Fetch cart data from backend
      fetch(`${process.env.REACT_APP_API_URL}/cart/show`, {
        headers: { 'Content-Type': 'application/json', Authorization: `Bearer ${localStorage.getItem('token')}` }
      })
        .then(res => res.json())
        .then(data => {
          setCart(data)
          console.log(data)
        })
    }

  const removeProduct = (productId) => {
    fetch(`${process.env.REACT_APP_API_URL}/cart/remove`, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json', Authorization: `Bearer ${localStorage.getItem('token')}`},
      body: JSON.stringify({
        productId: productId
      })
    })
    .then(res => res.json())
    .then(data => {
      if(data){
        Swal.fire("Product removed from cart")
        fetchCartData();
      }
      else{
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'There seems to be an error, please try again',
          footer: '<a href="">Why do I have this issue?</a>'
        })
      }
    })
  }


  const handleCheckout = () => {
    Swal.fire({
      title: 'Ready to checkout?',
      text: "You can still cancel your order in the orders tab",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: `Yes, I'm ready to checkout`
    }).then((result) => {
      if (result.isConfirmed) {
        // Fetch orders from backend
        fetch(`${process.env.REACT_APP_API_URL}/cart/checkOut`, {
          headers: {'Content-Type': 'application/json', Authorization: `Bearer ${localStorage.getItem('token')}`}     
        })
        .then(res => res.json())
        .then(data => {
          console.log(data)

          Swal.fire({
          position: 'middle',
          icon: 'success',
          title: 'Order has been sent to the seller!',
          showConfirmButton: false,
          timer: 1500
          })

          emptyCart();
          navigate('/orders')


        })

        }
      })
  } 
  if (!cart) {
    return <div style={styles.container}>Cart is empty</div>;
  }

  const emptyCart = () => {
    fetch(`${process.env.REACT_APP_API_URL}/cart/empty`, {
      method: 'DELETE',
      headers: {'Content-Type': 'application/json', Authorization: `Bearer ${localStorage.getItem('token')}`}
    })
    .then(res => res.json())
    .then(data => {
      console.log(data)
      fetchCartData();

    })
  }


  const handleEmpty = () => {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        emptyCart();
        Swal.fire(
          'Deleted!',
          'Your cart is empty, shop again!',
          'success'
        )

        navigate('/products')
      }
    })

  }

  return (
    <Container>
      <Row>
        <Col>
          <div style={styles.container}>
                <h1 style={styles.heading}>My Cart</h1>
                <div style={styles.info}>
                  <strong>User ID:</strong> {cart.userId}
                </div>
                <div style={styles.info}>
                  <strong>Total Price:</strong> ${cart.totalPrice}
                </div>
                <div style={styles.tableContainer}>
                    <div style={styles.horizontalScroll}>
                      <table style={styles.table}>
                        <thead>
                          <tr style={styles.tableRow}>
                            <th style={styles.tableHeader}>Product</th>
                            <th style={styles.tableHeader}>Product ID</th>
                            <th style={styles.tableHeader}>Product Name</th>
                            <th style={styles.tableHeader}>Quantity</th>
                            <th style={styles.tableHeader}>Price</th>
                            <th style={styles.tableHeader}>Subtotal</th>
                            <th style={styles.tableHeader}>Action</th> {/* Added Action column */}
                          </tr>
                        </thead>
                        <tbody>
                          {cart.products.map((product) => (
                            <tr key={product.productId} style={styles.tableRow}>
                              <td style={styles.tableCell}>
                                <img src={product.img} alt={product.productName} style={styles.productImage} />
                              </td>
                              <td style={styles.tableCell}>{product.productId}</td>
                              <td style={styles.tableCell}>{product.productName}</td>
                              <td style={styles.tableCell}>{product.quantity}</td>
                              <td style={styles.tableCell}>{product.price}</td>
                              <td style={styles.tableCell}>{product.subTotal}</td>
                              <td style={styles.tableCell}>
                                <Button variant="danger" onClick={() => removeProduct(product.productId)}>
                                  Remove
                                </Button>
                              </td>
                            </tr>
                          ))}
                        </tbody>
                      </table>
                  </div>
                </div>
                <div style={styles.checkoutButtonContainer}>
                  <Button variant="success" onClick={handleCheckout} style={styles.checkoutButton} className="mx-2">
                    Checkout
                  </Button>
                  <Button variant="danger" onClick={handleEmpty} style={styles.emptyButton} className="mx-2">
                    Empty Cart
                  </Button>
                </div>
              </div>
            </Col>
        </Row>
    </Container>
        );
}
// Custom styles
const styles = {
  container: {
      margin: '0 auto',
      maxWidth: '100%',
      padding: '16px',
      backgroundColor: '#f0f0f0',
      borderRadius: '8px',
    },
    heading: {
      marginBottom: '16px',
      textAlign: 'center',
      color: '#FF6F61',
      fontSize: '28px',
    },
    info: {
      marginBottom: '16px',
      color: '#6F6F6F',
      fontSize: '16px',
    },
    tableContainer: {
        overflowX: 'auto',
    },
    horizontalScroll: {
      overflowX: 'scroll',
      maxWidth: '100%',
    },
    table: {
      width: '100%',
      borderCollapse: 'collapse',
      marginTop: '16px',
    },
    tableRow: {
      backgroundColor: '#FFFFFF',
    },
    tableHeader: {
      padding: '12px',
      borderBottom: '1px solid #CCCCCC',
      textAlign: 'left',
      color: '#6F6F6F',
      fontSize: '16px',
    },
    tableCell: {
      padding: '12px',
      borderBottom: '1px solid #CCCCCC',
      textAlign: 'left',
      color:  '#6F6F6F',
    fontSize: '14px',
    },
    productImage: {
    width: '80px',
    height: '80px',
    objectFit: 'cover',
    borderRadius: '4px',
    },
    checkoutButtonContainer: {
    display: 'flex',
    justifyContent: 'center',
    marginTop: '16px',
    },
    checkoutButton: {
    minWidth: '150px',
    fontSize: '16px'
    }
    };

